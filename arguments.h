#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <string>
#include <vector>
#include <stdexcept>
#include <exception>
#include <cstring>
#include <sstream>

#include <arpa/inet.h>
#include <netdb.h>
#include <chrono> // for find out timestamp size
#include "data.h"


#include <iostream>


class Arg_parser
{
	tdata data;
	
	std::vector<std::string> arguments;
	
	void set_default_values();
	ip_type check_ip_address(std::string &address);
	double parse_rtt_node(std::string &address);
	int get_value(std::string &str);
	double get_floating_value(std::string &str);
	std::string get_address_from_host(struct hostent *host);	
public:
	Arg_parser(int argc, char **argv);
	tdata parse();
};

#endif