#include "udp_listener.h"

void udp_listener::listening(bool *error_result)
{
    int sock;
    struct sockaddr_in server, client;
    unsigned char buffer[BUFFER_SIZE];

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(port);

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        std::cerr << "UDP listner can't create socket: exiting listner thread" << std::endl;
        *error_result = true;
        return;
    }
    if (bind(sock, (struct sockaddr*)&server, sizeof(server))==-1)
    {
        std::cerr << "UDP listner can't bind to port: exiting listner thread" << std::endl;
        *error_result = true;
        return;
    }
    socklen_t addr_len = sizeof(client);
    
    do
    {
        std::memset(buffer, 0, BUFFER_SIZE);
        int length = recvfrom(sock, buffer, BUFFER_SIZE, MSG_DONTWAIT, (struct sockaddr *)&client, &addr_len);
        if (length > 0)
        {
            sendto(sock, buffer, length, 0, (struct sockaddr*)&client, addr_len);
        }
        usleep(1000); // wait one millisecond
    }
    while(!end_listening);
}
void udp_listener::end_message()
{
    end_listening = true;
}