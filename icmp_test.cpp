#include "icmp_test.h"


ICMP_testing::ICMP_testing(tdata &data): app_data(data)
{
	end_recv = false;
	end_send = false;
    critical_error = false;
}

void ICMP_testing::measure_loss()
{
	std::vector<std::thread*> threads_vector;

	int i = 1;
	for (const auto &x : app_data.ip_nodes)
	{
		std::stringstream ss;
		ss << "uzel" << i;
		n_data.push_back(new node_data());    //alloc data and invoke node process
		threads_vector.push_back(new std::thread(&ICMP_testing::node_process,this, x, ss.str()));
		i++;
	}
	//in the end join threads and free memory
	for (auto x = threads_vector.begin(); x != threads_vector.end(); ++x)
	{
		(*x)->join();
		delete (*x);
        *x = nullptr;
	}
    //cleaning
	for (auto x = n_data.begin(); x != n_data.end();++x)
	{
		delete (*x);
        *x = nullptr;
	}
    if (critical_error)
    {
        throw std::runtime_error(error_message);
    }
}

void ICMP_testing::node_process(ip_information ip_info, std::string _node_name)
{
	//deklaration variables
	int sock;
	IP_address *node_address = nullptr;
	std::string node_name;

    if (ip_info.host_name == "")
        node_name = _node_name;
    else
        node_name = ip_info.host_name;
    //set ip according to ip version (type)
	if (ip_info.type == IPV4_TYPE)
		node_address = new IPV4_address(ip_info.ip_address);		
	else
		node_address = new IPV6_address(ip_info.ip_address);

	sock = create_socket(node_address->get_af_type());
	if (!connection(node_address, sock))
    {
        active_error("Can't connect to server");
    }

	//unique identification for each node (and data node)
	unsigned short node_num = std::atoi(_node_name.substr(_node_name.size()-1,_node_name.size()).c_str());
	unsigned short pid = getpid() + node_num;
	//decrement node_num cause indexing started with zero
	node_num--;
	//init n_data
	n_data[node_num]->timeout = app_data.timeout;
    if (ip_info.rtt > 0.0)
        n_data[node_num]->rtt_threshold = ip_info.rtt;
    else
       n_data[node_num]->rtt_threshold = app_data.rtt;
	//generate random data
	n_data[node_num]->data = new unsigned char [app_data.data_size - TIME_SIZE];
	generate_data(n_data[node_num]->data, app_data.data_size - TIME_SIZE);

    //get current time and start testing (sending and recieving)
	n_data[node_num]->last_stat_time = std::chrono::system_clock::now();
	n_data[node_num]->last_stat_hour = n_data[node_num]->last_stat_time;
	std::thread sending(&ICMP_testing::sending_packets,this,sock,pid, node_num, node_address->get_af_type());
	std::thread recieving(&ICMP_testing::recieving_packets,this,sock,node_address,pid, node_name, node_num);
    //sync
	sending.join();
	recieving.join();

	//cleaning
	close(sock);
    if (node_address!=nullptr)
    {
	   delete node_address;
       node_address = nullptr;
    }
	if (n_data[node_num]->data != nullptr)
    {
		delete [] n_data[node_num]->data; 
        n_data[node_num]->data = nullptr;
    }
}
void ICMP_testing::sending_packets(int sock, unsigned short pid,unsigned short index, unsigned short af_domain)
{
	unsigned char packet_buffer[BUFFER_SIZE];
	while (!end_send)
	{	//active waiting
		if (!n_data[index]->doing_stat) //waiting for stats
		{
			send_packet(sock, packet_buffer, pid, index, af_domain);
			n_data[index]->send_attempts++;
			std::this_thread::sleep_for(std::chrono::milliseconds(app_data.send_interval));
		}
	}
}

void ICMP_testing::recieving_packets(int sock, IP_address *node_addr, unsigned short pid, std::string node_name,unsigned short index)
{
	unsigned char buffer[BUFFER_SIZE];
	
	while(!end_recv)
	{
		int length_packet = recieve_packet(sock, buffer, index);
		error_code result = packet_processing(buffer, length_packet, node_addr, pid, node_name, index, sock);

		switch (result)
		{
			case SUCCESS:
				n_data[index]->success_recv++;
				break;
			case TIMEOUT:
				n_data[index]->rtt_loss_recv++;
				break;
			case MISS:
				n_data[index]->lost_recv++;;
				break;
			case LOCALHOST_ECHO:
				//ignore and go up to cycle
				continue;
			default:
				break;
		}
        //get current time and compare if it's statistics time
		std::chrono::time_point<std::chrono::system_clock> current_time = std::chrono::system_clock::now();
		auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(current_time - n_data[index]->last_stat_time);
		auto seconds = std::chrono::duration_cast<std::chrono::seconds>(current_time - n_data[index]->last_stat_hour);
		bool normal_stats = (microseconds.count()/1000000.0) > app_data.loss_interval;
		bool hour_stats = seconds.count() > HOUR;

        //DO stats both or only one
		if (normal_stats && hour_stats) 
		{
			n_data[index]->doing_stat = true;
			int last_send_value = n_data[index]->send_attempts;
			//sync with sending packets
			std::this_thread::sleep_for(std::chrono::microseconds((app_data.send_interval*1000)/10));
			if (last_send_value != n_data[index]->send_attempts)
				n_data[index]->send_attempts--; //throw packet away

			do_loss_stats(index, node_name);
			do_hour_stats(index, node_name);
            //actualize old values
			n_data[index]->last_stat_time = current_time;
			n_data[index]->last_stat_hour = current_time;
			n_data[index]->doing_stat = false;
		}
		else if (normal_stats)
		{
			n_data[index]->doing_stat = true;
			int last_send_value = n_data[index]->send_attempts;
			//sync with sending packets
			std::this_thread::sleep_for(std::chrono::microseconds((app_data.send_interval*1000)/10));
			if (last_send_value != n_data[index]->send_attempts)
				n_data[index]->send_attempts--; //throw packet away

			do_loss_stats(index, node_name);
			n_data[index]->last_stat_time = current_time;
			n_data[index]->doing_stat = false;
		}		
		else if (hour_stats)
		{
            n_data[index]->doing_stat = true;
			int last_send_value = n_data[index]->send_attempts;
			//sync with sending packets
			std::this_thread::sleep_for(std::chrono::microseconds((app_data.send_interval*1000)/10));
			if (last_send_value != n_data[index]->send_attempts)
				n_data[index]->send_attempts--; //throw packet away
			
			do_hour_stats(index, node_name);
			n_data[index]->last_stat_hour = current_time;
			n_data[index]->doing_stat = false;
		}
	}
}

int ICMP_testing::recieve_packet(int sock, unsigned char *buffer, unsigned short &index)
{	
	int length = 0;
	int us_timeout = 0;
    //get timeout value and add interval sending instead first packet
	if (n_data[index]->send_attempts == 0)
		us_timeout = int(n_data[index]->timeout * 1000000);
	else
		us_timeout = int(n_data[index]->timeout * 1000000) + (app_data.send_interval * 1000);
	
	struct timeval timeout = {0, us_timeout};
	fd_set read_set;

	std::memset(&read_set, 0, sizeof(read_set));
	FD_SET(sock, &read_set);
    //active select
	int check_value = select(sock + 1, &read_set, nullptr, nullptr, &timeout);
	if (check_value == 0)
	{  //out of timeout
		return -2;
	}
	else if (check_value < 0)
	{
        active_error("Select failed");
	}

	length = recv(sock, buffer, BUFFER_SIZE, 0);

	return length;
}

error_code ICMP_testing::packet_processing(unsigned char *data, int length, IP_address *node, unsigned short pid, std::string &node_name, unsigned short &index, int &sock)
{
	if (length == -2)  //if recieve nothing, miss it
		return MISS;

    struct iphdr *ip;
    struct icmphdr *icmphead;
    unsigned ip_head_offset = 0; // for same work with packet in both version IP 
    int packet_bytes = 0;

    //get ip version
    if (node->get_af_type()==AF_INET)
    {
        //in version 4 recieve ip header
        ip = (struct iphdr*)data;
        ip_head_offset = ip->ihl * 4;
        icmphead = (struct icmphdr*)&data[ip_head_offset];
        packet_bytes = length - ip_head_offset;
    }
    else
    {   
        ip = nullptr;
        icmphead = (struct icmphdr*)&data[ip_head_offset];
        packet_bytes = length;
    }    
	//for testing local host and ping to own local address
	if ((node->get_string_addr() == "127.0.0.1" ||
		node->get_string_addr() == "::1" ||
        node->get_string_addr() == get_my_ipv4(sock)) &&
		(icmphead->type == ICMP_ECHO || icmphead->type == ICMPV6_ECHO))
	{
		return LOCALHOST_ECHO;
	}
    //check if it is THE packet
	bool condition = (icmphead->type == ICMP_ECHOREPLY ||
                     (node->get_af_type() == AF_INET6 && icmphead->type == ICMPV6_ECHOREPLY)) &&
		      		 icmphead->un.echo.id == pid &&
				     (memcmp(data + (ip_head_offset + sizeof(struct icmphdr) + TIME_SIZE), n_data[index]->data, app_data.data_size - TIME_SIZE) == 0);						
	if (condition)
	{
		double rtt_ms = 0.0;
        //get current time and calculate RTT value
		std::chrono::time_point<std::chrono::system_clock> *last_time = new std::chrono::time_point<std::chrono::system_clock>;
		std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
		std::memcpy((unsigned char *)last_time, &data[ip_head_offset + sizeof(struct icmphdr)], sizeof(now));
		auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(now - *last_time);
		rtt_ms = microseconds.count()/1000.0;
	    delete last_time;
        last_time = nullptr;

		if ((n_data[index]->timeout * 1000.0) < rtt_ms)
			return MISS; //it shouldn't be heppened, post checking if packet recv late
		
		if (n_data[index]->rtt_threshold > 0.0 && n_data[index]->rtt_threshold < rtt_ms)
               return TIMEOUT;  //rtt timeout 

        //set variables for stats
		if (n_data[index]->min_rtt > rtt_ms)
			n_data[index]->min_rtt = rtt_ms;
		if (n_data[index]->max_rtt < rtt_ms)
			n_data[index]->max_rtt = rtt_ms;
		n_data[index]->sum_rtt += rtt_ms;
		n_data[index]->each_rtt.push_back(rtt_ms);

		if (app_data.verbose_mode)
		{//print out in verbouse mode
            std::string time_str = get_current_time_str();
			//length bytes without IP header
			console_mutex.lock();
			std::cout << time_str << " "
					  << packet_bytes << " Bytes from "
					  << node_name << " (" << node->get_string_addr() << ") "
					  << "time=" << std::fixed << std::setprecision(3)
					  << rtt_ms << " ms" << std::endl;
			//return output format to default
			std::cout.unsetf(std::ios_base::floatfield);
			console_mutex.unlock();
		}
		//new timeout value 2xRTT
		n_data[index]->timeout = (rtt_ms * 2.0) / 1000.0;
		return SUCCESS;
	}
	return MISS;
}

void ICMP_testing::send_packet(int sock, unsigned char *packet, unsigned short pid, unsigned short index, unsigned short af_domain)
{
	std::memset(packet,0,BUFFER_SIZE);
	//data generator
	unsigned char *timestamp_data = new unsigned char[app_data.data_size];

	//my timestamp
	std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
	memcpy(timestamp_data,(unsigned char *)&now, TIME_SIZE);
	//fix data
	memcpy(&timestamp_data[TIME_SIZE], n_data[index]->data, (app_data.data_size - TIME_SIZE));
	
	//set header
	struct icmphdr *icmp_packet = new struct icmphdr;
	//find out ip version
    if (af_domain == AF_INET)
        icmp_packet->type = ICMP_ECHO;
    else
        icmp_packet->type = ICMPV6_ECHO;

	icmp_packet->code = 0;
	icmp_packet->un.echo.id = pid;
	icmp_packet->un.echo.sequence = n_data[index]->send_attempts;
	icmp_packet->checksum = 0;
	icmp_packet->checksum = checksum_data(icmp_packet,timestamp_data, app_data.data_size);

    //copy data to packet and send packet
	int offset = 0;
	std::memcpy(&packet[offset], (unsigned char *)icmp_packet, sizeof(struct icmphdr));
	offset += sizeof(struct icmphdr);
	std::memcpy(&packet[offset], timestamp_data, app_data.data_size);
	
	send(sock, packet, sizeof(struct icmphdr) + app_data.data_size, 0);
	//cleaning
	delete icmp_packet;
    icmp_packet = nullptr;
	delete [] timestamp_data;
    timestamp_data = nullptr;
}
void ICMP_testing::do_loss_stats(unsigned short &index, std::string &node_name)
{
	//if still packets has not been recieved so throw it up
	int diff = n_data[index]->send_attempts - (n_data[index]->lost_recv + n_data[index]->success_recv + n_data[index]->rtt_loss_recv);
	if (diff != 0)
	{
		n_data[index]->lost_recv += diff;
	}
    //prepare values for stats
    n_data[index]->timeout_attempts = n_data[index]->send_attempts - n_data[index]->timeout_attempts;
    n_data[index]->timeout_rtt_loss = n_data[index]->rtt_loss_recv - n_data[index]->timeout_rtt_loss;
    n_data[index]->timeout_loss = n_data[index]->lost_recv - n_data[index]->timeout_loss;
	//calculating
	double percent_lost = (n_data[index]->timeout_loss / double(n_data[index]->timeout_attempts)) * 100.0;
	double percent_rtt = (n_data[index]->timeout_rtt_loss / double(n_data[index]->timeout_attempts)) * 100.0;

	std::string time_str = get_current_time_str();
    //printing out
    if (percent_lost > 0.0)
    {
    	console_mutex.lock();
    	std::cout << time_str << " " << node_name << ": " <<
    		std::fixed << std::setprecision(3) << percent_lost << "% packet loss, ";
    	std::cout.unsetf(std::ios_base::floatfield);
    	std::cout << n_data[index]->timeout_loss << " packet lost" << std::endl;
    	console_mutex.unlock();
    }
    if (percent_rtt > 0.0)
    {
        console_mutex.lock();
        std::cout << time_str << " " << node_name << ": " << 
            std::fixed << std::setprecision(3) << percent_rtt << "% ";
            std::cout.unsetf(std::ios_base::floatfield);
        std::cout << "(" << n_data[index]->timeout_rtt_loss << ") packets exceeded RTT threshold "
            << n_data[index]->rtt_threshold << " ms" << std::endl;
        console_mutex.unlock();
    }
    //actualize values
    n_data[index]->timeout_attempts = n_data[index]->send_attempts;
    n_data[index]->timeout_rtt_loss = n_data[index]->rtt_loss_recv;
    n_data[index]->timeout_loss = n_data[index]->lost_recv;
}
void ICMP_testing::do_hour_stats(unsigned short &index, std::string &node_name)
{
	//if still packets has not been recieved so throw it up
	int diff = n_data[index]->send_attempts - (n_data[index]->lost_recv + n_data[index]->success_recv + n_data[index]->rtt_loss_recv);
	if (diff != 0)
	{
		n_data[index]->lost_recv += diff;
	}
    //prepare values
    n_data[index]->hourtest_attempts = n_data[index]->send_attempts - n_data[index]->hourtest_attempts;
    n_data[index]->hourtest_success = n_data[index]->success_recv - n_data[index]->hourtest_success;
    n_data[index]->hourtest_loss = n_data[index]->lost_recv - n_data[index]->hourtest_loss;

	double percent_lost = (n_data[index]->hourtest_loss / double(n_data[index]->hourtest_attempts)) * 100.0;
    
    std::string time_str = get_current_time_str();

	console_mutex.lock();
	std::cout << time_str << " " << node_name << ": ";
	if (percent_lost ==100.0)
		std::cout << "status down" << std::endl;
	else
	{//calculating RTT stats and printing out
		double avg_rtt = n_data[index]->sum_rtt/double (n_data[index]->hourtest_success);
		double sum_mdev = 0.0;
		for (auto x = n_data[index]->each_rtt.begin(); x != n_data[index]->each_rtt.end(); ++x)
		{
			sum_mdev += std::pow((*x - avg_rtt),2);
		}
		double mdev = std::sqrt(sum_mdev/(double(n_data[index]->hourtest_success)-1.0));
		std::cout << percent_lost << "% packet loss, rtt min/avg/max/mdev " <<
			std::fixed << std::setprecision(3) << n_data[index]->min_rtt << "/" <<
			avg_rtt << "/" << n_data[index]->max_rtt << "/" << mdev << std::endl;
			std::cout.unsetf(std::ios_base::floatfield);
	}
	console_mutex.unlock();
    //actualize values
    n_data[index]->max_rtt = 0.0;
    n_data[index]->min_rtt = 9999999999.0;
    n_data[index]->sum_rtt = 0.0;
    n_data[index]->each_rtt.clear();
    n_data[index]->hourtest_attempts = n_data[index]->send_attempts;
    n_data[index]->hourtest_success = n_data[index]->success_recv;
    n_data[index]->hourtest_loss = n_data[index]->lost_recv;
}
unsigned short ICMP_testing::checksum_data(struct icmphdr *head, unsigned char *data, int lenght_data)
{
	char *packet = new char[BUFFER_SIZE];
	char *ptr = nullptr;
	ptr = packet;
	int len_packet = 0;
    //fill temporary packet
	std::memcpy(ptr, &head->type, sizeof(head->type));
	ptr += sizeof(head->type);
	len_packet += sizeof(head->type);
	std::memcpy(ptr, &head->code, sizeof(head->code));
	ptr += sizeof(head->code);
	len_packet += sizeof(head->code);
	std::memcpy(ptr, &head->checksum, sizeof(head->checksum));
	ptr += sizeof(head->checksum);
	len_packet += sizeof(head->checksum);
	std::memcpy(ptr, &head->un.echo.id, sizeof(head->un.echo.id));
	ptr += sizeof(head->un.echo.id);
	len_packet += sizeof(head->un.echo.id);
	std::memcpy(ptr, &head->un.echo.sequence, sizeof(head->un.echo.sequence));
	ptr += sizeof(head->un.echo.sequence);
	len_packet += sizeof(head->un.echo.sequence);
    //copy data
	std::memcpy(ptr, data, lenght_data);
	ptr += lenght_data;
	len_packet += lenght_data;
    //rounding packet data
	for (int i = 0; i < lenght_data%2; ++i, ptr++)
	{
		*ptr = 0;
		ptr++;
		len_packet++;
	}
    //calculating real checksum
	unsigned short check_sum = checksum((unsigned char*)packet, len_packet);
    //cleaning
	delete [] packet;
    packet = nullptr;
	return check_sum;
}
//get socket function
int ICMP_testing::create_socket(unsigned short af_domain)
{
	int sock = -1;

	if (AF_INET == af_domain)
		sock = socket(af_domain, SOCK_RAW, IPPROTO_ICMP);
	else
		sock = socket(af_domain, SOCK_RAW, IPPROTO_ICMPV6);
	
	if (sock == -1)
    {
        active_error("Socket has not been created. Try start app like root user");
    }

	return sock;
}
bool ICMP_testing::connection(IP_address *ip, int sock)
{
	//Connect for comunication with only one ip node
	if (connect(sock, ip->get_sockaddr(), ip->get_size_of_struct()) == -1)
		return false;
	return true;
}
void ICMP_testing::end_message()
{
	end_recv = true;
	std::this_thread::sleep_for(std::chrono::milliseconds(app_data.send_interval));
	end_send = true;
}
void ICMP_testing::active_error(std::string message)
{
    //if error is already set so will not set again
    if (!critical_error)
    {
        //end testing
        end_send = true;
        end_recv = true;
        //set error
        critical_error = true;
        error_message = message;
    }
}