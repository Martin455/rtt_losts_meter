#ifndef CONFIG_DATA_H
#define CONFIG_DATA_H

#include <vector>
#include <string>
#include <cstdlib>
#include <cstring>
#include <chrono>
#include <ctime>

#include <iostream>

#define BUFFER_SIZE 8192 //max buffer size
#define HOUR 3600 //in [s] 

#define TIME_SIZE (sizeof(std::chrono::time_point<std::chrono::system_clock>))

enum error_code {
	SUCCESS = 0,
	TIMEOUT,
	MISS,
	LOCALHOST_ECHO,
};

enum ip_type {
	IPV4_TYPE,
	IPV6_TYPE,
	INVALID_TYPE
};
//information about node
struct ip_information {
	std::string ip_address;
	ip_type type; //type 0 or 1 setting
	std::string host_name;
	double rtt;
	ip_information(std::string addr, ip_type t, std::string hn, double _rtt)
	{
		ip_address = addr;
		type = t;
		host_name = hn;
		rtt = _rtt;
	}
};
//main application data
struct tdata
{
	int data_size; //[B] default 56B for udp 64B
	int loss_interval; //[s] default 300s
	int send_interval; //[ms] default 100ms
	double timeout; //[s] default 2s or 2xrtt
	int udp_port; 
	int listening_udp_port;
	double rtt; //in [ms]
	bool udp;
	bool verbose_mode; //app print packets to stdout
	bool help_flag; //only for print help

	std::vector<ip_information> ip_nodes;
	tdata()
	{
		udp = false;
		verbose_mode = false;
		help_flag = false;

		data_size = -1;
		loss_interval = 300;
		send_interval = 100;
		timeout = -1.0;
		udp_port = -1;
		listening_udp_port = -1;
		rtt = -1.0;
	}

	tdata(const tdata &data) //copy constructor
	{
		data_size = data.data_size;
        loss_interval = data.loss_interval;
		send_interval = data.send_interval;
		timeout = data.timeout;
		udp_port = data.udp_port;
		listening_udp_port = data.listening_udp_port;
		rtt = data.rtt;
		udp = data.udp;
		verbose_mode = data.verbose_mode;
		help_flag = data.help_flag;

		ip_nodes = data.ip_nodes;
	}
};
//Data for each node
struct node_data {
	//info
	int send_attempts;
	int success_recv;
	int rtt_loss_recv;
	int lost_recv;
	double max_rtt; //rtt in [ms]
	double min_rtt;
	double sum_rtt;
	double rtt_threshold;
	double timeout; //[s]
	//for synchronization
	bool doing_stat; 
	//stats
	std::chrono::time_point<std::chrono::system_clock> last_stat_time;
	std::chrono::time_point<std::chrono::system_clock> last_stat_hour;
	std::vector<double> each_rtt;

	int timeout_attempts;
	int timeout_rtt_loss;
	int timeout_loss;

	int hourtest_attempts;
	int hourtest_success;
	int hourtest_loss;
	
	unsigned char *data;
	node_data(): send_attempts(0), success_recv(0),
		rtt_loss_recv(0), lost_recv(0), max_rtt(0.0),
		min_rtt(9999999999999.0), sum_rtt(0.0), rtt_threshold(0.0),
		doing_stat(false), timeout_attempts(0), timeout_rtt_loss(0),
		timeout_loss(0), hourtest_attempts(0), hourtest_success(0),
		hourtest_loss(0)		
	{
		data = nullptr;
	}
};
//get current time in format string
inline std::string get_current_time_str()
{
    time_t t = std::time(nullptr);
    struct tm *tt = std::localtime(&t);
    char t_buffer[64];
    std::strftime(t_buffer,sizeof(t_buffer),"%Y-%m-%d %H:%M:%S", tt);
    std::string time_str(t_buffer);

    return time_str;
}

/***
* Check sum counting for TCP/IP packets
* Source: https://locklessinc.com/articles/tcp_checksum
* **/
inline unsigned short checksum(const unsigned char *packet, unsigned size)
{
	unsigned sum = 0;
	unsigned i = 0;
	// Accumulate checksum
	for (; i < size - 1; i += 2)
	{
		unsigned short word16 = *(unsigned short *) &packet[i];
		sum += word16;
	}
	// Handle odd-sized case
	if (size & 1)
	{
		unsigned short word16 = (unsigned char) packet[i];
		sum += word16;
	}
	// Fold to get the ones-complement result 
	while (sum >> 16) sum = (sum & 0xFFFF)+(sum >> 16);

	// Invert to get the negative in ones-complement arithmetic 
	return ~sum;
}
//generate data, size in Byte
inline void generate_data(unsigned char *buffer,int size)
{
	if (size == 0)
		return;

	int length = size/sizeof(int);
	int *array = new int[length];

	for (int i = 0; i < length; ++i)
		array[i] = rand();

	std::memcpy(buffer,(unsigned char*)array, size);

	delete [] array;
	array = nullptr;
}

#endif // CONFIG_DATA