#include "arguments.h"

Arg_parser::Arg_parser(int argc, char **argv)
{
	// in argv[0] is app name 
	for (int i = 1; i < argc; ++i)
	{
		arguments.push_back(std::string(argv[i]));
	}
}

tdata Arg_parser::parse()
{
 	for (unsigned i = 0; i < arguments.size(); ++i)
 	{
 		if (arguments.at(i) == "-h")
 		{
			data.help_flag = true;
			break; 			
 		}
 		else if (arguments.at(i) == "-u")
 		{
 			data.udp = true;
 		}
 		else if (arguments.at(i) == "-s")
 		{
 			i++; 			
 			data.data_size = get_value(arguments.at(i));
 		}
 		else if (arguments.at(i) == "-t")
 		{
 			i++;
 			data.loss_interval = get_value(arguments.at(i));
 		}
 		else if (arguments.at(i) == "-i")
 		{
 			i++;
 			data.send_interval = get_value(arguments.at(i));
 		}
 		else if (arguments.at(i) == "-p")
 		{
 			i++;
 			data.udp_port = get_value(arguments.at(i));
 		}
 		else if (arguments.at(i) == "-l")
 		{
 			i++;
 			data.listening_udp_port = get_value(arguments.at(i));
 		}
 		else if (arguments.at(i) == "-r")
 		{
 			i++;
 			data.rtt = get_floating_value(arguments.at(i));
 		}
 		else if (arguments.at(i) == "-v")
 		{
 			data.verbose_mode = true;
 		}
 		else if (arguments.at(i) == "-w")
 		{
 			i++;
 			data.timeout = get_floating_value(arguments.at(i));
 		}
 		else
 		{
 			struct hostent *host;

 			std::string address = arguments.at(i);
 			double rtt = parse_rtt_node(address);	//parse rtt node extension or return 0
 			ip_type type = check_ip_address(address);	//check address and get version ip
 			if (type == INVALID_TYPE)
 			{//if wrong address format so check hostname
 				if ((host = gethostbyname(address.c_str())) == nullptr)
 				{
 					throw std::runtime_error("Wrong address or host");
 				}
 				std::string addr = get_address_from_host(host);
 				
 				data.ip_nodes.push_back(ip_information(addr,IPV4_TYPE,address,rtt));
 			}
 			else
 			{
 				data.ip_nodes.push_back(ip_information(address, type,"",rtt));	
 			}
 		}
 	}
 	//throw errors or set default values
 	if (data.udp && data.udp_port==-1)
 		throw std::runtime_error("Can't set UDP without port");

	if (data.help_flag && arguments.size() > 1)
		throw std::runtime_error("Too many arguments");

	set_default_values();

 	return data;
}

void Arg_parser::set_default_values()
{
	//timestamp size for calculate min data
	int timestamp_size = sizeof(std::chrono::time_point<std::chrono::system_clock>);
	if (data.data_size==-1)
		data.data_size = data.udp ? 64 : 56;
	else if (data.data_size < timestamp_size)
		data.data_size = timestamp_size;	//for ICMP timestamp
	else if (data.data_size > (BUFFER_SIZE - 32)) //32 is max ICMP header
		data.data_size = (BUFFER_SIZE - 32);
	
	//-0.5 value because double has not to be equal -1.0000... 
	if (data.timeout < -0.5)
	{
		if (data.rtt < -0.5)
			data.timeout = 2.0;
		else
			data.timeout = data.rtt * 2.0;
	}
}

ip_type Arg_parser::check_ip_address(std::string &address)
{
	ip_type result = INVALID_TYPE;

	struct sockaddr_in sa;

	//validation ipv4 address
	int validation = inet_pton(AF_INET, address.c_str(), &(sa.sin_addr));
	if (validation!=0)
		result = IPV4_TYPE;
	else
	{
		//validation ipv6 address
		validation = inet_pton(AF_INET6, address.c_str(), &(sa.sin_addr));
		if (validation!=0)
			result = IPV6_TYPE;
	}

	return result;
}
double Arg_parser::parse_rtt_node(std::string &address)
{
	double rtt = 0.0;
	size_t found_semi = address.find(";");	//find separator
	if (found_semi == std::string::npos)
		return rtt;
	//get rtt value and ip or hostname
	std::string rtt_str = address.substr(found_semi + 1, address.size());
	address = address.substr(0, found_semi);
	rtt = get_floating_value(rtt_str);

	return rtt;
}
//parse string to int or error
int Arg_parser::get_value(std::string &str)
{
	char *endPtr;
	int return_value = std::strtol(str.c_str(),&endPtr,10);
	if (*endPtr != '\0')
		throw std::runtime_error("Argument wrong: can't parse number");
	return return_value;
}
//parse string to float or error
double Arg_parser::get_floating_value(std::string &str)
{
	char *endPtr;
	double return_value = std::strtod(str.c_str(), &endPtr);
	if (*endPtr != '\0')
		throw std::runtime_error("Argument wrong: can't parse floating number");
	return return_value;
}
//Get string address from host struct
std::string Arg_parser::get_address_from_host(struct hostent *host)
{
	std::stringstream ss;
	for (int i = 0; i < host->h_length; ++i)
	{
		if (host->h_addrtype == AF_INET)
		{
			unsigned n = (unsigned char)host->h_addr_list[0][i];
			ss << n
			   << ((i==(host->h_length-1)) ? "" : ".");
		}
	}
	return ss.str();
}
