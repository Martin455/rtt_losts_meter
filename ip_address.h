#ifndef IP_ADDRESS_H
#define IP_ADDRESS_H

#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string>

/* ***
* Class for encapsulating work with IP in both version
* It is only abstract class
* **/
class IP_address
{
public:
	IP_address() = default;
	virtual ~IP_address() = default;

	virtual struct hostent *get_host() = 0;
	virtual struct sockaddr *get_sockaddr() = 0;
	virtual unsigned get_size_of_struct() = 0;
	virtual unsigned short get_af_type() = 0;
	virtual int get_port() = 0;
	virtual std::string get_string_addr() = 0;

	virtual void set_port(int port) = 0;
};

//functions get primary IP for compare if icmp packet is not type ECHO
//unfortunately is not implement for ipv6
//so maybe it will be bug with testing own ipv6 address
inline std::string get_my_ipv4(int sock)
{
    struct sockaddr_in name;
    socklen_t namelen = sizeof(name);
    getsockname(sock, (sockaddr*) &name, &namelen);

    return std::string(inet_ntoa(name.sin_addr));
}

#endif