#ifndef ICMP_TESTING_H
#define ICMP_TESTING_H

//threading and timing
#include <thread>
#include <chrono>
#include <ctime>
#include <mutex>
#include <atomic>

//network work
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <netdb.h>

//representation IP addr
#include "ipv4_address.h"
#include "ipv6_address.h"

//for closing socket
#include <unistd.h>

//for print out info and memory copy
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <iomanip> //for printing floating point number

//math for statistics
#include <cmath>

//data for app
#include <vector>
#include "data.h"

//ICMPv6 types
#define ICMPV6_ECHO 128
#define ICMPV6_ECHOREPLY 129

class ICMP_testing
{
	//data only for reading
	//so threads can approach at the same time
	const tdata app_data; 
	//foreach node data
	std::vector<node_data*> n_data;

	//for end app detection
	//and error detection
	bool end_recv;
	bool end_send;
	bool critical_error;
	std::string error_message;
	void active_error(std::string message);
	//writing out data
	std::mutex console_mutex;
	//for each node its own thread
	void node_process(ip_information ip_info, std::string _node_name);
	//socket functions
	int create_socket(unsigned short af_domain);
	bool connection(IP_address *ip, int sock);
	void send_packet(int sock, unsigned char *packet, unsigned short pid, unsigned short index, unsigned short af_domain);
	//recieve packet and return length packet
	int recieve_packet(int sock, unsigned char *buffer, unsigned short &index);
	error_code packet_processing(unsigned char *data, int length, IP_address *node, unsigned short pid, std::string &node_name, unsigned short &index, int &sock);	

	//threading functions
	void sending_packets(int sock, unsigned short pid, unsigned short index, unsigned short af_domain);
	void recieving_packets(int sock, IP_address *node_addr, unsigned short pid, std::string node_name,unsigned short index);
	
	unsigned short checksum_data(struct icmphdr *head, unsigned char *data, int lenght_data);
	
	//statistics
	void do_loss_stats(unsigned short &index, std::string &node_name);
	void do_hour_stats(unsigned short &index, std::string &node_name);
public:
	ICMP_testing(tdata &data);

	void measure_loss(); //invoke threads for nodes

	void end_message(); // for end cycles 
};


#endif