#ifndef UDP_LISTENER_H
#define UDP_LISTENER_H

//network
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/ip.h>

#include <cstring>
#include <iostream>

#include "data.h"

class udp_listener
{
private:
    int port;
    bool end_listening;
public:
    udp_listener(int _port): port(_port)
    {
        end_listening = false;
    }
    //Inspiration from UDP server in ISA examples
    void listening(bool *error_result);
    //message from main when user put ctrl + c
    void end_message();    
};

#endif