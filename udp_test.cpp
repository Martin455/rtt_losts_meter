#include "udp_test.h"

void UDP_testing::measure_loss()
{
    std::vector<std::thread*> threads_vector;
    int i = 1;
    for (const auto &x : app_data.ip_nodes)
    {
        std::stringstream ss;
        ss << "uzel" << i;  //get node name and node number
        n_data.push_back(new node_data());  //init data for node
        threads_vector.push_back(new std::thread(&UDP_testing::node_process,this, x, ss.str())); //run node thread
        i++;
    }
    //Cleaning after testing
    for (auto x = threads_vector.begin(); x != threads_vector.end(); ++x)
    {
        (*x)->join();
        delete (*x);
    }
    for (auto x = n_data.begin(); x != n_data.end();++x)
    {
        delete (*x);
    }
    if (critical_error)
    {
        throw std::runtime_error(error_message);
    }
}
void UDP_testing::node_process(ip_information ip_info, std::string _node_name)
{
    IP_address *address;
    std::string node_name;
    if (ip_info.host_name == "")
        node_name = _node_name;
    else
        node_name = ip_info.host_name;
    //for indexing decrease node_num
    unsigned short node_num = std::atoi(_node_name.substr(_node_name.size()-1,_node_name.size()).c_str());
    node_num--;

    if (ip_info.type == IPV4_TYPE)
        address = new IPV4_address(ip_info.ip_address);        
    else
        address = new IPV6_address(ip_info.ip_address);

    int sock = create_and_connect(address);
    n_data[node_num]->data = new unsigned char [(app_data.data_size - TIME_SIZE)];
    generate_data(n_data[node_num]->data, (app_data.data_size - TIME_SIZE));
    //Initialization data for stats
    n_data[node_num]->timeout = app_data.timeout;
    n_data[node_num]->rtt_threshold = app_data.rtt;
    n_data[node_num]->last_stat_hour = std::chrono::system_clock::now();
    n_data[node_num]->last_stat_time = n_data[node_num]->last_stat_hour;

    //run sending and recieving threads
    std::thread send_thread(&UDP_testing::sending,this,sock,node_num);
    std::thread recv_thread(&UDP_testing::recieving,this,sock,node_num,node_name, address->get_string_addr());

    //cleaning
    send_thread.join();
    recv_thread.join();
    close(sock);
    if (address != nullptr)
        delete address;
    delete [] n_data[node_num]->data;
}
int UDP_testing::create_and_connect(IP_address *ip)
{
    int sock = -1;
    ip->set_port(app_data.udp_port);

    if (AF_INET == ip->get_af_type())
    {
        sock = socket(AF_INET, SOCK_DGRAM, 0);
    }
    else
    {
        sock = socket(AF_INET6, SOCK_DGRAM, 0);
    }

    if (sock==-1)
    {
        active_error("UDP socket has not been created");
    }

    if (connect(sock, ip->get_sockaddr(), ip->get_size_of_struct())==-1)
    {
        active_error("UDP connection has not been created");
    }

    return sock;
}

void UDP_testing::sending(int sock, unsigned short index)
{
    unsigned char packet[BUFFER_SIZE];
    while(!end_send)
    {
        if (!n_data[index]->doing_stat) // sync with counting and printing stats and stop sending packets
        {
            send_packet(sock, packet, index);
            n_data[index]->send_attempts++;
            std::this_thread::sleep_for(std::chrono::milliseconds(app_data.send_interval));
        }
    }
}
void UDP_testing::send_packet(int sock, unsigned char *packet, unsigned short &index)
{
    std::memset(packet,0,BUFFER_SIZE);
    //set timestamp to UDP packet
    std::chrono::time_point<std::chrono::system_clock> current_time = std::chrono::system_clock::now();
    std::memcpy(packet, (unsigned char*)&current_time, TIME_SIZE);
    //copy random data to UDP packet
    std::memcpy(packet + TIME_SIZE, n_data[index]->data, app_data.data_size - TIME_SIZE);
    send(sock, packet, app_data.data_size, 0);
}
void UDP_testing::recieving(int sock, unsigned short index, std::string node_name, std::string ip_str)
{
    unsigned char buffer[BUFFER_SIZE];
    while(!end_recv)
    {
        error_code code = MISS;
        int length_packet = recv_packet(sock, buffer, index);
        if (length_packet > 0)
        {
            code = process_packet(buffer, length_packet, index, node_name, ip_str);
        }
        switch (code)
        {
            case SUCCESS:
                n_data[index]->success_recv++;
                break;
            case TIMEOUT:
                n_data[index]->rtt_loss_recv++;
                break;
            case MISS:
                n_data[index]->lost_recv++;
                break;
            default:
                break;
        }
        //get current time and compare with stats time
        std::chrono::time_point<std::chrono::system_clock> current_time = std::chrono::system_clock::now();
        auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(current_time - n_data[index]->last_stat_time);
        auto seconds = std::chrono::duration_cast<std::chrono::seconds>(current_time - n_data[index]->last_stat_hour);
        bool normal_stats = (microseconds.count()/1000000.0) > app_data.loss_interval;
        bool hour_stats = seconds.count() > HOUR;

        if (normal_stats && hour_stats)
        {//doing stats
            n_data[index]->doing_stat = true;
            int last_send_value = n_data[index]->send_attempts;
            //sync with sending packets
            std::this_thread::sleep_for(std::chrono::microseconds((app_data.send_interval*1000)/10));
            if (last_send_value != n_data[index]->send_attempts)
                n_data[index]->send_attempts--; //throw packet away

            do_timeout_stats(index, node_name);
            do_hour_stats(index, node_name);
            //actualize last time
            n_data[index]->last_stat_time = current_time;
            n_data[index]->last_stat_hour = current_time;
            n_data[index]->doing_stat = false;
        }
        else if (normal_stats)
        {
            n_data[index]->doing_stat = true;
            int last_send_value = n_data[index]->send_attempts;
            //sync with sending packets
            std::this_thread::sleep_for(std::chrono::microseconds((app_data.send_interval*1000)/10));
            if (last_send_value != n_data[index]->send_attempts)
                n_data[index]->send_attempts--; //throw packet away

            do_timeout_stats(index, node_name);
            n_data[index]->last_stat_time = current_time;
            n_data[index]->doing_stat = false;
        }       
        else if (hour_stats)
        {
            n_data[index]->doing_stat = true;
            int last_send_value = n_data[index]->send_attempts;
            //sync with sending packets
            std::this_thread::sleep_for(std::chrono::microseconds((app_data.send_interval*1000)/10));
            if (last_send_value != n_data[index]->send_attempts)
                n_data[index]->send_attempts--; //throw packet away
            do_hour_stats(index, node_name);
            n_data[index]->last_stat_hour = current_time;
            n_data[index]->doing_stat = false;
        }
    }
}
error_code UDP_testing::process_packet(unsigned char *packet, int length, unsigned short &index, std::string &node_name, std::string &ip_str)
{
    double rtt_ms = 0.0;
    //get current time and timestamp from packet and calculate RTT value
    std::chrono::time_point<std::chrono::system_clock> current_time = std::chrono::system_clock::now();
    std::chrono::time_point<std::chrono::system_clock> *last_time = new std::chrono::time_point<std::chrono::system_clock>;
    std::memcpy((unsigned char*)last_time, packet, TIME_SIZE);
    auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(current_time - *last_time);
    rtt_ms = microseconds.count()/1000.0;
    delete last_time;

    //check RTT with threshold
    if (n_data[index]->rtt_threshold > 0.0 && n_data[index]->rtt_threshold < rtt_ms)
        return TIMEOUT;

    if (length == app_data.data_size)
    {
        if (std::memcmp(packet + TIME_SIZE, n_data[index]->data, app_data.data_size - TIME_SIZE) == 0) // check data in packet
        {
            //recording data for stats
            if (n_data[index]->min_rtt > rtt_ms)
                n_data[index]->min_rtt = rtt_ms;
            if (n_data[index]->max_rtt < rtt_ms)
                n_data[index]->max_rtt = rtt_ms;
            n_data[index]->sum_rtt += rtt_ms;
            n_data[index]->each_rtt.push_back(rtt_ms);

            if (app_data.verbose_mode)
            {
                std::string time_str = get_current_time_str();
                //print out info about packet
                console_mutex.lock();
                std::cout << time_str << " " << length << " Bytes from " <<
                    node_name << " (" << ip_str << ":" << app_data.udp_port << ") time=" <<
                    std::fixed << std::setprecision(3) << rtt_ms << " ms" << std::endl;
                std::cout.unsetf(std::ios_base::floatfield);
                console_mutex.unlock();
            }
            return SUCCESS;
        }
    } 
    return MISS;
}
int UDP_testing::recv_packet(int sock, unsigned char *buffer, unsigned short &index)
{
    int length_packet = 0;
    int us_timeout = 0;
    //setting time for waiting for packet
    if (n_data[index]->send_attempts == 0)  // first timeout is only timeout value
        us_timeout = int(n_data[index]->timeout * 1000000);
    else    //another timeout included sending time
        us_timeout = int(n_data[index]->timeout * 1000000) + (app_data.send_interval * 1000);
    
    struct timeval timeout = {0, us_timeout};
    fd_set read_set;

    std::memset(&read_set, 0, sizeof(read_set));
    FD_SET(sock, &read_set);
    int check = select(sock + 1,&read_set,nullptr,nullptr,&timeout);
    if (check==0)
        return -2;
    else if (check < 0)
    {
        active_error("Select failed");
    }
    length_packet = recv(sock, buffer, BUFFER_SIZE, 0);
    return length_packet;
}
//end cykles
void UDP_testing::end_message()
{
    end_recv = true;
    std::this_thread::sleep_for(std::chrono::milliseconds(app_data.send_interval));
    end_send = true;
}
void UDP_testing::do_timeout_stats(unsigned short &index, std::string &node_name)
{
    //sync hack for packet if was send after start stats [NOTE: more info in documentation]
    int diff = n_data[index]->send_attempts - (n_data[index]->lost_recv + n_data[index]->success_recv + n_data[index]->rtt_loss_recv);
    if (diff != 0)
    {
        n_data[index]->lost_recv += diff;
    }
    //get values from last interval 
    n_data[index]->timeout_attempts = n_data[index]->send_attempts - n_data[index]->timeout_attempts;
    n_data[index]->timeout_rtt_loss = n_data[index]->rtt_loss_recv - n_data[index]->timeout_rtt_loss;
    n_data[index]->timeout_loss = n_data[index]->lost_recv - n_data[index]->timeout_loss;

    double percent_lost = (n_data[index]->timeout_loss / double(n_data[index]->timeout_attempts)) * 100.0;
    double percent_rtt = (n_data[index]->timeout_rtt_loss / double(n_data[index]->timeout_attempts)) * 100.0;

    std::string time_str = get_current_time_str();

    //print out values
    if (percent_lost > 0.0)
    {
        console_mutex.lock();
        std::cout << time_str << " " << node_name << ": " <<
            std::fixed << std::setprecision(3) << percent_lost << "% packet loss, ";
        std::cout.unsetf(std::ios_base::floatfield);
        std::cout << n_data[index]->timeout_loss << " packet lost" << std::endl;
        console_mutex.unlock();
    }
    if (percent_rtt > 0.0)
    {
        console_mutex.lock();
        std::cout << time_str << " " << node_name << ": " << 
            std::fixed << std::setprecision(3) << percent_rtt << "% ";
            std::cout.unsetf(std::ios_base::floatfield);
        std::cout << "(" << n_data[index]->timeout_rtt_loss << ") packets exceeded RTT threshold "
            << n_data[index]->rtt_threshold << " ms" << std::endl;
        console_mutex.unlock();
    }
    //actualize old values
    n_data[index]->timeout_attempts = n_data[index]->send_attempts;
    n_data[index]->timeout_rtt_loss = n_data[index]->rtt_loss_recv;
    n_data[index]->timeout_loss = n_data[index]->lost_recv;
}
void UDP_testing::do_hour_stats(unsigned short &index, std::string &node_name)
{
    int diff = n_data[index]->send_attempts - (n_data[index]->lost_recv + n_data[index]->success_recv + n_data[index]->rtt_loss_recv);
    if (diff != 0)
    {
        n_data[index]->lost_recv += diff;
    }

    n_data[index]->hourtest_attempts = n_data[index]->send_attempts - n_data[index]->hourtest_attempts;
    n_data[index]->hourtest_success = n_data[index]->success_recv - n_data[index]->hourtest_success;
    n_data[index]->hourtest_loss = n_data[index]->lost_recv - n_data[index]->hourtest_loss;

    double percent_lost = (n_data[index]->hourtest_loss / double(n_data[index]->hourtest_attempts)) * 100.0;
    
    std::string time_str = get_current_time_str();

    console_mutex.lock();
    std::cout << time_str << " " << node_name << ": ";
    if (percent_lost ==100.0)
        std::cout << "status down" << std::endl;
    else
    {   //calculate average and deviation
        double avg_rtt = n_data[index]->sum_rtt/double (n_data[index]->hourtest_success);
        double sum_mdev = 0.0;
        for (auto x = n_data[index]->each_rtt.begin(); x != n_data[index]->each_rtt.end(); ++x)
        {
            sum_mdev += std::pow((*x - avg_rtt),2);
        }
        double mdev = std::sqrt(sum_mdev/(double(n_data[index]->hourtest_success)-1.0));
        //print out values
        std::cout << percent_lost << "% packet loss, rtt min/avg/max/mdev " <<
            std::fixed << std::setprecision(3) << n_data[index]->min_rtt << "/" <<
            avg_rtt << "/" << n_data[index]->max_rtt << "/" << mdev << std::endl;
            std::cout.unsetf(std::ios_base::floatfield);
    }
    console_mutex.unlock();
    //actualize last values and prepare to next stats
    n_data[index]->max_rtt = 0.0;
    n_data[index]->min_rtt = 9999999999.0;
    n_data[index]->sum_rtt = 0.0;
    n_data[index]->each_rtt.clear();
    n_data[index]->hourtest_attempts = n_data[index]->send_attempts;
    n_data[index]->hourtest_success = n_data[index]->success_recv;
    n_data[index]->hourtest_loss = n_data[index]->lost_recv;
}
void UDP_testing::active_error(std::string message)
{//if error is already activated so it's not necessary active another
    if (!critical_error)
    {
        //turn off cycles
        end_send = true;
        end_recv = true;
        //active error and set message
        critical_error = true;
        error_message = message;
    }
}