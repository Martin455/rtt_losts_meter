/***
** RTT loss meter Application
** Project for ISA
** Author Martin Prajka
***/

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <csignal>
#include <thread>
#include <unistd.h>
#include "arguments.h"
#include "icmp_test.h"
#include "udp_test.h"
#include "udp_listener.h"

void print_help();
void handlerInterupt(int s);
void clean();

//Main classes in project
ICMP_testing *icmp_test = nullptr;
UDP_testing *udp_test = nullptr;
udp_listener *listener = nullptr;

int main(int argc, char **argv)
{
    //Error with low priority 
    //if is true and something crash in testing return value will be set to another crash
    bool udp_listener_error = false;
	//setting seed for random data
    srand(time(nullptr));
	signal(SIGINT, handlerInterupt);
	try
	{
        //Parse arguments and get app_data
		Arg_parser parser(argc, argv);
		tdata app_data = parser.parse();
        std::thread *listen_udp_thread = nullptr;

		if (app_data.help_flag)
		{
			print_help();
			return 0;
		}
        if (app_data.listening_udp_port != -1)
        {
            listener = new udp_listener(app_data.listening_udp_port);
            listen_udp_thread = new std::thread(&udp_listener::listening,listener, &udp_listener_error);
        }
        if (app_data.udp)
        {
            udp_test = new UDP_testing(app_data);
            udp_test->measure_loss();
        }
        else
        {
            icmp_test = new ICMP_testing(app_data);
            icmp_test->measure_loss();    
        }
        if (listen_udp_thread!=nullptr)
        {
            if (listen_udp_thread->joinable())
                listen_udp_thread->join();
            delete listen_udp_thread;
            listen_udp_thread = nullptr;
        }
        clean();
	} 
	catch (std::exception &e)
	{
		std::cerr << "App failed with exception message:\n" <<
			e.what() << std::endl;
        clean();
		return 1;
	}
    if (udp_listener_error)
        return 2;

	return 0;
}

void print_help()
{
	std::cout << "Help for testovac app." << std::endl <<
    "Testovac is tool for monitoring and availability net nodes." << std::endl;
	std::cout << "Using: " << "./testovac [-h] [-u] [-t <interval>] [-i <interval>] "
			  << "[-p <port>] [-l <port>] [-s <size>] [-r <value>] "
			  << "<node1> <node2> <node3> ..." << std::endl;
	std::cout << "Arguments description:" << std::endl;
	std::cout << "\t-h - Print this help" << std::endl;
	std::cout << "\t-u - UDP protocol for testing" << std::endl;
	std::cout << "\t-s - Data size, default value for ICMP 56B and 64B for UDP (in Byte)" << std::endl;
	std::cout << "\t-t <interval> -- interval for evaluation lossability, default 300s (in seconds)" << std::endl;
	std::cout << "\t-i <interval> -- sending interval, default 100 ms (in milliseconds)" << std::endl;
	std::cout << "\t-w <timeout> -- time for waiting for answer, default 2s or 2xRTT (in seconds)" << std::endl;
	std::cout << "\t-p <port> -- UDP port for testing" << std::endl;
	std::cout << "\t-l <port> -- UDP port for listening" << std::endl;
	std::cout << "\t-r <value> -- RTT value (in seconds)" << std::endl;
	std::cout << "\t-v - verbose mode for printing packets" << std::endl;
	std::cout << "\t<node> -- IPv4, IPv6 or hostname" << std::endl;
    std::cout << "\tOr you can put \"node;rtt\" and node will be its own rtt threshold" << std::endl;
    std::cout << "Note: for UDP testing and listener user can't be root and for ICMP testing user have to be root." << std::endl;
}

void handlerInterupt(int s)
{
	//hack warning compiler
	(void)s;

	//sending end messages
    if (icmp_test!=nullptr)
        icmp_test->end_message();
    if (udp_test!=nullptr)
        udp_test->end_message();
    if (listener!=nullptr)
        listener->end_message();
    usleep(2500); //sync sleep 
}
void clean()
{
    if (icmp_test!=nullptr)
    {
        delete icmp_test;
        icmp_test = nullptr;
    }
    if (udp_test!=nullptr)
    {
        delete udp_test;
        udp_test = nullptr;
    }
    if (listener!=nullptr)
    {
        delete listener;
        listener = nullptr;
    }
}
