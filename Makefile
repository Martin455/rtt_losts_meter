#Makefile for RTT loss meter
#project name "testovac"
#autor Martin Prajka

CC=g++
CPPFLAGS=-std=c++11 -g -Wall -Wextra -pedantic

LIBRARIES=-pthread -lm
PROJECTNAME=testovac
OBJFILES=main.o arguments.o icmp_test.o udp_test.o ipv4_address.o ipv6_address.o udp_listener.o
HEADERFILES=arguments.h icmp_test.h udp_test.h ipv4_address.h ipv6_address.h ip_address.h udp_listener.h

PACKAGENAME=xprajk00.tar

.PHONY: clean

all: $(PROJECTNAME)

udp_listener.o: udp_listener.cpp $(HEADERFILES)
	$(CC) $(CPPFLAGS) -c udp_listener.cpp -o udp_listener.o

ipv6_address.o: ipv6_address.cpp $(HEADERFILES)
	$(CC) $(CPPFLAGS) -c ipv6_address.cpp -o ipv6_address.o

ipv4_address.o: ipv4_address.cpp $(HEADERFILES)
	$(CC) $(CPPFLAGS) -c ipv4_address.cpp -o ipv4_address.o

udp_test.o: udp_test.cpp $(HEADERFILES)
	$(CC) $(CPPFLAGS) -c udp_test.cpp -o udp_test.o $(LIBRARIES)

icmp_test.o: icmp_test.cpp $(HEADERFILES)
	$(CC) $(CPPFLAGS) -c icmp_test.cpp -o icmp_test.o $(LIBRARIES)

arguments.o: arguments.cpp $(HEADERFILES)
	$(CC) $(CPPFLAGS) -c arguments.cpp -o arguments.o

main.o: main.cpp $(HEADERFILES)
	$(CC) $(CPPFLAGS) -c main.cpp -o main.o $(LIBRARIES)

$(PROJECTNAME): $(OBJFILES)
	$(CC) $(CPPFLAGS) -o $(PROJECTNAME) $(OBJFILES) $(LIBRARIES)

clean:
	rm -rf *.o $(PROJECTNAME)

pack: clean
	tar -cf $(PACKAGENAME) *.cpp *.h Makefile testovac.1 manual.pdf
