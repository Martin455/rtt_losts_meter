#ifndef IPV4_ADDRESS_H
#define IPV4_ADDRESS_H

#include "ip_address.h"

class IPV4_address: public IP_address
{
	struct sockaddr_in addr;
public:
	IPV4_address(std::string &address);

	virtual struct hostent *get_host();
	virtual struct sockaddr *get_sockaddr();
	virtual unsigned get_size_of_struct();
	virtual unsigned short get_af_type();
	virtual int get_port();
	virtual std::string get_string_addr();

	virtual void set_port(int port);
};

#endif