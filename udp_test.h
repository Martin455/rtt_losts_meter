#ifndef UDP_TESTING_H
#define UDP_TESTING_H

//threading and timing
#include <thread>
#include <chrono>
#include <ctime>
#include <mutex>
#include <atomic>

//network work
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

//representation IP addr
#include "ipv4_address.h"
#include "ipv6_address.h"

//I/O and string
#include <iostream>
#include <sstream>
#include <cstring>
#include <stdexcept>
#include <exception>
#include <iomanip> 

//stats
#include <cmath>

//data
#include <vector>
#include "data.h"

class UDP_testing
{
    //global data
    const tdata app_data;
    //data foreach node
    std::vector<node_data*> n_data;
    //synchronization
    std::atomic<bool> end_recv;
    std::atomic<bool> end_send;
    std::mutex console_mutex;
    //Error detection
    bool critical_error;
    std::string error_message;
    void active_error(std::string message);
	//socket functions
    int create_and_connect(IP_address *ip);
    
    //main functions (thread functions)
    void node_process(ip_information ip_info, std::string _node_name);
    void sending(int sock, unsigned short index);
    void recieving(int sock, unsigned short index, std::string node_name, std::string ip_str);
    //packet function
    int recv_packet(int sock, unsigned char *buffer, unsigned short &index);
    void send_packet(int sock, unsigned char *packet, unsigned short &index);
    error_code process_packet(unsigned char *packet, int length, unsigned short &index, std::string &node_name, std::string &ip_str);
    //statistics function
    void do_timeout_stats(unsigned short &index, std::string &node_name);
    void do_hour_stats(unsigned short &index, std::string &node_name);
public:
    UDP_testing(tdata &data): app_data(data)
    {
        end_recv = false;
        end_send = false;
    }

    void measure_loss();
    void end_message();
};

#endif
