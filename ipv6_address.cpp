#include "ipv6_address.h"

IPV6_address::IPV6_address(std::string &address)
{
	addr.sin6_family = AF_INET6;
	addr.sin6_port = 0;
	inet_pton(AF_INET6, address.c_str(), &addr.sin6_addr);
}

struct hostent *IPV6_address::get_host()
{
	return gethostbyaddr((char*)&addr.sin6_addr, sizeof(addr.sin6_addr), addr.sin6_family);
}

unsigned IPV6_address::get_size_of_struct()
{
	return sizeof(struct sockaddr_in6);
}

struct sockaddr *IPV6_address::get_sockaddr()
{
	return (struct sockaddr*)&addr;
}

unsigned short IPV6_address::get_af_type()
{
	return addr.sin6_family;
}

int IPV6_address::get_port()
{
	return addr.sin6_port;
}

void IPV6_address::set_port(int port)
{
	addr.sin6_port = port;
}

std::string IPV6_address::get_string_addr()
{
	char destination[INET6_ADDRSTRLEN];
	
	inet_ntop(AF_INET6,&addr.sin6_addr,destination,INET6_ADDRSTRLEN);
	std::string str(destination);

	return str;
}