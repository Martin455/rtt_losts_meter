#include "ipv4_address.h"

IPV4_address::IPV4_address(std::string &address)
{
	addr.sin_family = AF_INET;
	addr.sin_port = 0;
	inet_aton(address.c_str(), &addr.sin_addr);
}

struct hostent *IPV4_address::get_host()
{
	return gethostbyaddr((char*)&addr.sin_addr, sizeof(addr.sin_addr), addr.sin_family);
}

unsigned IPV4_address::get_size_of_struct()
{
	return sizeof(struct sockaddr_in);
}

struct sockaddr *IPV4_address::get_sockaddr()
{
	return (struct sockaddr*)&addr;
}

unsigned short IPV4_address::get_af_type()
{
	return addr.sin_family;
}

int IPV4_address::get_port()
{
	return addr.sin_port;
}

void IPV4_address::set_port(int port)
{
	addr.sin_port = htons(port);
}

std::string IPV4_address::get_string_addr()
{
	std::string str(inet_ntoa(addr.sin_addr));
	return str;
}